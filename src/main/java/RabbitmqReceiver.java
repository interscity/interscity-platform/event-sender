import com.rabbitmq.client.*;
import com.twitter.bijection.Injection;
import com.twitter.bijection.avro.GenericAvroCodecs;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericData.Record;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.io.JsonEncoder;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.avro.specific.SpecificRecord;

import java.io.*;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.TimeoutException;

public class RabbitmqReceiver implements Receiver{
    private Channel channel;
    private String queue_name;

    private Set<String> URLs;
    private HttpRequest request;
    private HttpClient httpClient;


    public void updateURLs(Set<String> urls){
        URLs.clear();
        URLs = urls;
    }



    public RabbitmqReceiver(ConnectionFactory factory, String eventId, Schema schema, Set<String> urls) {
        URLs = urls;
        final Schema scheme = schema;
        httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_1_1)  // this is the default
                .build();

        try {
            //Connection connection = factory.newConnection();
            Connection conn = factory.newConnection();
            channel = conn.createChannel();
            String exchange_name = "EXCHANGE";
            channel.exchangeDeclare(exchange_name, "topic");
            queue_name = channel.queueDeclare().getQueue();
            channel.queueBind(queue_name, exchange_name, eventId);
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body){

                String evt = AvroDeserialize(body,scheme).toString();

                System.out.println(evt);
                for (String url : URLs) {
                    //System.out.print(url+json.toString()+"\n");
                    request =  HttpRequest.newBuilder()
                            .timeout(Duration.ofMinutes(1))
                            .header("Content-Type", "application/json")
                            .uri(URI.create(url))
                            .POST(HttpRequest.BodyPublishers.ofByteArray(Jsoncoverter(AvroDeserialize(body,scheme))))
                            .build();
                    try {
                        httpClient.send(request, HttpResponse.BodyHandlers.ofString());
                    } catch (IOException | InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        try {
            channel.basicConsume(queue_name, true, "myConsumerTag", consumer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void CloseConnection()  {
        try {
            channel.close();
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }


    private static Record AvroDeserialize(byte [] bytes, Schema schema){
        Injection<Record, byte[]> recordInjection;
        recordInjection = GenericAvroCodecs.toBinary(schema);
        return recordInjection.invert(bytes).get();
    }

    private static byte[] Jsoncoverter(GenericData.Record event){
        EncoderFactory ef = new EncoderFactory();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        JsonEncoder jsonEncoder = null;
        try {
            jsonEncoder = ef.jsonEncoder(event.getSchema(),stream,true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        DatumWriter<GenericRecord> writer = event instanceof SpecificRecord ?
                new SpecificDatumWriter<>(event.getSchema()) :
                new GenericDatumWriter<>(event.getSchema());
        try {
            writer.write(event, jsonEncoder);
            jsonEncoder.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stream.toByteArray();

    }

}
